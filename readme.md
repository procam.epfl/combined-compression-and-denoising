### Combined compression and denoising in the ProCam framework

#### Enviroment

The whole experiment is run under python 3.7 and pyTorch 1.9 on Raspberry Pi device.

#### File Description

1. ```CompressAI-master.zip``` : Code from CompressAI library (https://github.com/InterDigitalInc/CompressAI)

2. ```noise.py``` : Code for adding gaussian noise to the images

3. ```tensorboard.py``` : Code for running tensorboard to check the training loss

4. ```camera.py``` : Code to control the pi camera to capture images
5. ```check_psnr.py``` : Function to calculate psnr
6. ```retrain.ipynb``` : Code to retrain the model in CompressAI library. The retrained models are in CompressAI-master/compressai/zoo
7. ```plot.py``` : Code to draw psnr/ms-ssim plot
8. ```test_compress_viz.py``` : Code to run the model in CompressAI library