import numpy as np
import random
import cv2
import os

def noisy(image,sigma):
   
	row,col,ch= image.shape
	mean = 0
	  #var = 0.1
	  #sigma = var**0.5
	gauss = np.random.normal(mean,sigma,(row,col,ch))
	gauss = gauss.reshape(row,col,ch)
	noisy = image + gauss
	return noisy

for i in range(1,32):
	img = cv2.imread(str(i)+'.png')
	for j in range(5,25,5):
		noise = noisy(img, j)
		cv2.imwrite(str(i)+'_gaus'+str(j)+'.png', noise)