import math
import io
import torch
import os
from torchvision import transforms
import numpy as np
from scipy.interpolate import interp1d
from PIL import Image
#from fastai.vision.all import show_image
#import matplotlib.pyplot as plt
from pytorch_msssim import ms_ssim

from compressai.zoo import bmshj2018_hyperprior
#from ipywidgets import interact, widgets

import matplotlib.pyplot as plt


def compute_psnr(a, b):
    mse = torch.mean((a - b)**2).item()
    return -10 * math.log10(mse)
def compute_msssim(a, b):
    return ms_ssim(a, b, data_range=1.).item()

def compute_bpp(out_net):
    size = out_net['x_hat'].size()
    num_pixels = size[0] * size[2] * size[3]
    return sum(torch.log(likelihoods).sum() / (-math.log(2) * num_pixels)
              for likelihoods in out_net['likelihoods'].values()).item()

device = 'cuda' if torch.cuda.is_available() else 'cpu'
networks = {
'quality_1': bmshj2018_hyperprior(quality=1, pretrained=True).eval().to(device),
'quality_2':bmshj2018_hyperprior(quality=2, pretrained=True).eval().to(device),
'quality_3':bmshj2018_hyperprior(quality=3, pretrained=True).eval().to(device),
'quality_4':bmshj2018_hyperprior(quality=4, pretrained=True).eval().to(device),
'quality_5':bmshj2018_hyperprior(quality=5, pretrained=True).eval().to(device),
'quality_6':bmshj2018_hyperprior(quality=6, pretrained=True).eval().to(device),
#'quality_7':bmshj2018_hyperprior(quality=7, pretrained=True).eval().to(device),
#'quality_8':bmshj2018_hyperprior(quality=8, pretrained=True).eval().to(device)

}

img = Image.open('31_gaus10.png').convert('RGB')

newsize = (1920,1088)
img = img.resize(newsize)
x = transforms.ToTensor()(img).unsqueeze(0).to(device)

img2 = Image.open('31.png').convert('RGB')
newsize = (1920,1088)
img2 = img2.resize(newsize)
x2 = transforms.ToTensor()(img2).unsqueeze(0).to(device)

# img3 = Image.open('11.png').convert('RGB')
# newsize = (1920,1088)
# img3 = img3.resize(newsize)
# x3 = transforms.ToTensor()(img3).unsqueeze(0).to(device)


# def get_num_pixels(filepath):
#     width, height = Image.open(filepath).size
#     return width*height
# device = 'cuda' if torch.cuda.is_available() else 'cpu'
# net = bmshj2018_hyperprior(quality=3, pretrained=True).eval().to(device)

# with torch.no_grad():
#     out_net = net.forward(x)

# out_net['x_hat'].clamp_(0, 1)

# ans = {
#         'psnr': compute_psnr(x, x2),
#         'ms-ssim': compute_msssim(x, x2),
#         # 'bit-rate': compute_bpp(out_net)
#     }
# print(ans)
# print(hi)
outputs = {}
with torch.no_grad():
   for name, net in networks.items():
       rv = net(x)
       rv['x_hat'].clamp_(0, 1)
       outputs[name] = rv


#metrics = {}
count = 0 
A_x = []
A_y = []
B_x = []
B_y = []

for name, out in outputs.items():
    count+=1
    print(count)
    if count<4:
        A_x.append(compute_bpp(out))
        A_y.append(compute_psnr(x2, out["x_hat"]))
        print(compute_bpp(out))
    else:
        B_x.append(compute_bpp(out))
        B_y.append(compute_psnr(x2, out["x_hat"]))
        print(compute_bpp(out))
   # metrics[name] = {
   #     'psnr': compute_psnr(x2, out["x_hat"]),
   #     #'ms-ssim': compute_msssim(x2, out["x_hat"]),
   #     'bit-rate': compute_bpp(out),
   # }
#print(metrics)
#metrics = {'quality_1': {'psnr': 28.64012573989184, 'ms-ssim': 0.9178635478019714, 'bit-rate': 0.12019370496273041}, 'quality_2': {'psnr': 29.5573682661295, 'ms-ssim': 0.9269393086433411, 'bit-rate': 0.16532158851623535}, 'quality_3': {'psnr': 30.320465626894546, 'ms-ssim': 0.935258686542511, 'bit-rate': 0.23191505670547485}, 'quality_4': {'psnr': 31.071523707165998, 'ms-ssim': 0.9479048252105713, 'bit-rate': 0.37862372398376465}, 'quality_5': {'psnr': 32.44674514503395, 'ms-ssim': 0.969633162021637, 'bit-rate': 0.7229512333869934}, 'quality_6': {'psnr': 34.8587146876302, 'ms-ssim': 0.9844062924385071, 'bit-rate': 1.3222858905792236}, 'quality_7': {'psnr': 37.378557971617646, 'ms-ssim': 0.9909628033638, 'bit-rate': 1.9566659927368164}, 'quality_8': {'psnr': 39.73259169129268, 'ms-ssim': 0.9942924380302429, 'bit-rate': 2.5265004634857178}}

#metrics = {'quality_1': {'psnr': 30.123989887480278, 'ms-ssim': 0.9487159848213196, 'bit-rate': 19.660587310791016}, 'quality_2': {'psnr': 31.558744011063414, 'ms-ssim': 0.9570508599281311, 'bit-rate': 19.678516387939453}, 'quality_3': {'psnr': 32.85007689167168, 'ms-ssim': 0.964189350605011, 'bit-rate': 19.68979835510254}, 'quality_4': {'psnr': 33.95702762813264, 'ms-ssim': 0.9712695479393005, 'bit-rate': 19.712215423583984}, 'quality_5': {'psnr': 35.07993873515354, 'ms-ssim': 0.9780533909797668, 'bit-rate': 19.76752281188965}, 'quality_6': {'psnr': 36.37962730986112, 'ms-ssim': 0.9841394424438477, 'bit-rate': 32.90904998779297}, 'quality_7': {'psnr': 37.98502596977861, 'ms-ssim': 0.9897498488426208, 'bit-rate': 32.960514068603516}}

#fig, axes = plt.subplots(1, 2, figsize=(13, 5))
#plt.figtext(.5, 0., '(upper-left is better)', fontsize=12, ha='center')
#for name, m in metrics.items():
#    axes[0].plot(m['bit-rate'], m['psnr'], 'o', label=name)
#    axes[0].legend(loc='best')
#    axes[0].grid()
#    axes[0].set_ylabel('PSNR [dB]')
#    axes[0].set_xlabel('Bit-rate [bpp]')
#    axes[0].title.set_text('PSNR comparison')


# )
# C_x = np.array([0.08749856054782867,  0.13493001461029053, 0.2553810775279999, 0.6069524884223938, 1.1638062000274658, 2.2419357299804688, 2.9287312030792236,3.402918815612793 ]
# 	)
# C_y = np.array([0.7803304195404053, 0.7881267666816711, 0.8065011501312256,  0.8457736968994141,  0.8842799067497253, 0.9182403683662415, 0.9340671896934509, 0.94188684225082]

# 	)
# E_x = np.array([0.09421201050281525, 0.1854211390018463, 0.4514499306678772, 0.9948920011520386, 1.402421474456787, 2.6135802268981934, 3.2740838527679443,3.744483470916748]
# 	)
# E_y = np.array([0.7115134596824646, 0.7275144457817078,  0.7809107899665833, 0.8353001475334167, 0.8639463782310486, 0.9059122204780579, 0.930178165435791,0.930178165435791 ]




# )

# D_y = np.array([31.154135010504127]

# 	)
# Plotting the Graph
#x_new = np.linspace(A_x.min(), A_x.max(),500)

#f = interp1d(A_x, A_y, kind='quadratic')
#y_smooth=f(x_new)

# plt.plot (A_x,A_y, label="Noise_free vs Compress_Noise_free")
# plt.scatter (A_x, A_y)
# plt.plot (B_x,B_y, label="Noisy vs Compress_Noisy")
# plt.scatter (B_x, B_y)
# plt.plot (C_x,C_y, label="Noise_free vs Compress_Noisy")
# plt.scatter (C_x, C_y)


plt.plot (A_x,A_y, label="new model")
plt.scatter (A_x, A_y)
plt.plot (B_x,B_y, label="original model")
plt.scatter (B_x, B_y)
# plt.plot (C_x,C_y, label="gaussian sigma 15")
# plt.scatter (C_x, C_y)
# plt.plot (E_x,E_y, label="gaussian sigma 20")
# plt.scatter (E_x, E_y)

# plt.axhline(y=D_y, color='r', linestyle='-')
# plt.plot (D_y, label="Noisy vs Noise_free")
#plt.scatter (19.71613311767578, 30.30718562680754, color='r')
# plt.scatter (19.718103408813477, 30.801698590333316, color='r')
# plt.scatter (19.715917587280273, 30.774268032431284, color='r')
# plt.scatter (19.715553283691406, 30.697986518368452, color='r')
# plt.scatter (19.71590805053711, 30.560053735067108, color='r')

#plt.scatter (19.725473403930664, 30.165141240714128, color='b')
# plt.scatter (19.72757911682129, 30.653560850140444, color='b')
# plt.scatter (19.726587295532227, 30.575469864986967, color='b')
# plt.scatter (19.72597885131836, 30.501355867318782, color='b')
# plt.scatter (19.726802825927734, 30.30677785085298, color='b')



plt.xlabel('Bit-rate [bpp]')
plt.ylabel('PSNR[dB]')
plt.title('PSNR comparison')
#plt.xlim(0.0, 1.0)
#plt.title('MS-SSIM(log) comparison')
plt.legend()
#plt.show()
plt.savefig('hehe13.png')
#plt.savefig('comparePi_psnr_data31g10_retrain.png')
#print(torch.min(test))
#print(torch.max(test))

#print(torch.min(test))
#print(torch.max(test))
