import math
import io
import torch
import os
from torchvision import transforms
import numpy as np
from scipy.interpolate import interp1d
from PIL import Image
#from fastai.vision.all import show_image
#import matplotlib.pyplot as plt
from pytorch_msssim import ms_ssim

from compressai.zoo import bmshj2018_hyperprior
#from ipywidgets import interact, widgets

import matplotlib.pyplot as plt


def compute_psnr(a, b):
    mse = torch.mean((a - b)**2).item()
    return -10 * math.log10(mse)
def compute_msssim(a, b):
    return ms_ssim(a, b, data_range=1.).item()

def compute_bpp(out_net):
    size = out_net['x_hat'].size()
    num_pixels = size[0] * size[2] * size[3]
    return sum(np.log(likelihoods).sum() / (-math.log(2) * num_pixels)
              for likelihoods in out_net['likelihoods'].values()).item()


device = 'cuda' if torch.cuda.is_available() else 'cpu'
networks = {
#'quality_1': bmshj2018_hyperprior(quality=1, pretrained=True).eval().to(device),
#'quality_2':bmshj2018_hyperprior(quality=2, pretrained=True).eval().to(device),
#'quality_3':bmshj2018_hyperprior(quality=3, pretrained=True).eval().to(device),
#'quality_4':bmshj2018_hyperprior(quality=4, pretrained=True).eval().to(device),
#'quality_5':bmshj2018_hyperprior(quality=5, pretrained=True).eval().to(device),
#'quality_6':bmshj2018_hyperprior(quality=6, pretrained=True).eval().to(device),
#'quality_7':bmshj2018_hyperprior(quality=7, pretrained=True).eval().to(device),
#'quality_8':bmshj2018_hyperprior(quality=8, pretrained=True).eval().to(device)


}
for i in range(1,32):
	img = Image.open('dataset3/'+str(i)+'.png').convert('RGB')

	newsize = (1920,1088)
	img = img.resize(newsize)
	x = transforms.ToTensor()(img).unsqueeze(0).to(device)

	img2 = Image.open('dataset3_noise/'+str(i)+'_n.png').convert('RGB')
	newsize = (1920,1088)
	img2 = img2.resize(newsize)
	x2 = transforms.ToTensor()(img2).unsqueeze(0).to(device)

	# img3 = Image.open('11.png').convert('RGB')
	# newsize = (1920,1088)
	# img3 = img3.resize(newsize)
	# x3 = transforms.ToTensor()(img3).unsqueeze(0).to(device)


	# def get_num_pixels(filepath):
	#     width, height = Image.open(filepath).size
	#     return width*height

	ans = {
			 'image': i,
	         'psnr': compute_psnr(x2, x),
	#         'ms-ssim': compute_msssim(x2, x3),
	#         #'bit-rate': os.path.getsize('11.png')/get_num_pixels("1.png"),
	     }
	print(ans)
#outputs = {}
#with torch.no_grad():
#    for name, net in networks.items():
#        rv = net(x)
#        rv['x_hat'].clamp_(0, 1)
#        outputs[name] = rv


#metrics = {}
#for name, out in outputs.items():
#    metrics[name] = {
#        'psnr': compute_psnr(xx, out["x_hat"]),
#        'ms-ssim': compute_msssim(xx, out["x_hat"]),
#        'bit-rate': compute_bpp(out),
#    }
#print(metrics)
#metrics = {'quality_1': {'psnr': 28.64012573989184, 'ms-ssim': 0.9178635478019714, 'bit-rate': 0.12019370496273041}, 'quality_2': {'psnr': 29.5573682661295, 'ms-ssim': 0.9269393086433411, 'bit-rate': 0.16532158851623535}, 'quality_3': {'psnr': 30.320465626894546, 'ms-ssim': 0.935258686542511, 'bit-rate': 0.23191505670547485}, 'quality_4': {'psnr': 31.071523707165998, 'ms-ssim': 0.9479048252105713, 'bit-rate': 0.37862372398376465}, 'quality_5': {'psnr': 32.44674514503395, 'ms-ssim': 0.969633162021637, 'bit-rate': 0.7229512333869934}, 'quality_6': {'psnr': 34.8587146876302, 'ms-ssim': 0.9844062924385071, 'bit-rate': 1.3222858905792236}, 'quality_7': {'psnr': 37.378557971617646, 'ms-ssim': 0.9909628033638, 'bit-rate': 1.9566659927368164}, 'quality_8': {'psnr': 39.73259169129268, 'ms-ssim': 0.9942924380302429, 'bit-rate': 2.5265004634857178}}

#metrics = {'quality_1': {'psnr': 30.123989887480278, 'ms-ssim': 0.9487159848213196, 'bit-rate': 19.660587310791016}, 'quality_2': {'psnr': 31.558744011063414, 'ms-ssim': 0.9570508599281311, 'bit-rate': 19.678516387939453}, 'quality_3': {'psnr': 32.85007689167168, 'ms-ssim': 0.964189350605011, 'bit-rate': 19.68979835510254}, 'quality_4': {'psnr': 33.95702762813264, 'ms-ssim': 0.9712695479393005, 'bit-rate': 19.712215423583984}, 'quality_5': {'psnr': 35.07993873515354, 'ms-ssim': 0.9780533909797668, 'bit-rate': 19.76752281188965}, 'quality_6': {'psnr': 36.37962730986112, 'ms-ssim': 0.9841394424438477, 'bit-rate': 32.90904998779297}, 'quality_7': {'psnr': 37.98502596977861, 'ms-ssim': 0.9897498488426208, 'bit-rate': 32.960514068603516}}

#fig, axes = plt.subplots(1, 2, figsize=(13, 5))
#plt.figtext(.5, 0., '(upper-left is better)', fontsize=12, ha='center')
#for name, m in metrics.items():
#    axes[0].plot(m['bit-rate'], m['psnr'], 'o', label=name)
#    axes[0].legend(loc='best')
#    axes[0].grid()
#    axes[0].set_ylabel('PSNR [dB]')
#    axes[0].set_xlabel('Bit-rate [bpp]')
#    axes[0].title.set_text('PSNR comparison')

# Plotting the Graph
#x_new = np.linspace(A_x.min(), A_x.max(),500)

#f = interp1d(A_x, A_y, kind='quadratic')
#y_smooth=f(x_new)

# plt.plot (A_x,A_y, label="Noise_free vs Compress_Noise_free")
# plt.scatter (A_x, A_y)
# plt.plot (B_x,B_y, label="Noisy vs Compress_Noisy")
# plt.scatter (B_x, B_y)
# plt.plot (C_x,C_y, label="Noise_free vs Compress_Noisy")
# plt.scatter (C_x, C_y)

