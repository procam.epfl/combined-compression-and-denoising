import math
import io
import torch
from torchvision import transforms
import numpy as np

from PIL import Image
#from fastai.vision.all import show_image
#import matplotlib.pyplot as plt
from pytorch_msssim import ms_ssim

from compressai.zoo import bmshj2018_hyperprior
#from ipywidgets import interact, widgets

import matplotlib.pyplot as plt

# change tensor to image
def show(*imgs, i):

    img_idx = 0
    for img in imgs:
        img_idx +=1
        plt.figure(img_idx)
        if isinstance(img, torch.Tensor):
            img = img.detach().cpu()

            if img.dim()==3: # 3D tensor
                bz = 1
                c = img.shape[0]
                if c == 1:  # grayscale
                    img=img.squeeze()
                else:
                    raise Exception("unsupported type!  " + str(img.size()))
            else:
                raise Exception("unsupported type!  "+str(img.size()))


            img = img.numpy()  # convert to numpy
            np.savetxt('my_filepc.txt', img)
            img = img.squeeze()
            if bz ==1:
                plt.imsave('pcHatX_'+ str(i)+'.png', img, cmap='gray')

        else:
            raise Exception("unsupported type:  "+str(type(img)))




device = 'cuda' if torch.cuda.is_available() else 'cpu'
net = bmshj2018_hyperprior(quality=5, pretrained=True).eval().to(device)

img = Image.open('21_n.png').convert('RGB')
newsize = (1920,1088)
img = img.resize(newsize)
x = transforms.ToTensor()(img).unsqueeze(0).to(device)

with torch.no_grad():
    out_net = net.forward(x)
print(out_net.keys())

out_net['x_hat'].clamp_(0, 1)
rec_net = transforms.ToPILImage()(out_net['x_hat'].squeeze().cpu())
rec_net.save("21_new.png")

#print(torch.min(test))
#print(torch.max(test))

#test = (test-torch.min(test)) / (torch.max(test) - torch.min(test))

#print(torch.min(test))
#print(torch.max(test))

#for i in range(1):
    #test1 = torch.reshape(test[1], (1,68,120))
#    test1 = torch.reshape(test[0], (1,1088,1920))
    #every related to z
    #test1 = torch.reshape(test[i], (1,17,30))
#    show(test1, i=i)


#https://stackoverflow.com/questions/53623472/how-do-i-display-a-single-image-in-pytorch
